﻿using Newtonsoft.Json;
using Saber_Client.Pojo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saber_Client.Page
{
    public partial class ServerSetting : Form
    {
        public ServerSetting()
        {
            InitializeComponent();
            ServerHostInput.Text = Main.globalData._serverHost;
            AccessTokenInput.Text = Main.globalData._accessToken;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ServerHostInput.Text))
            {
                MessageBox.Show("Server URL不能为空");
                return;
            }

            if (string.IsNullOrEmpty(AccessTokenInput.Text))
            {
                MessageBox.Show("Access Token不能为空");
                return;
            }
            Main.globalData._serverHost = ServerHostInput.Text;
            Main.globalData._accessToken = AccessTokenInput.Text;
            DialogResult result = MessageBox.Show("保存成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                var globalData = new GlobalData
                {
                    _accessToken = AccessTokenInput.Text,
                    _serverHost = ServerHostInput.Text
                };
                string cacheJSON = JsonConvert.SerializeObject(globalData);
                File.WriteAllText("setting.json", cacheJSON);
                this.Close();
            }
        }
    }
}
