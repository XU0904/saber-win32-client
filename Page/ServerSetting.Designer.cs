﻿namespace Saber_Client.Page
{
    partial class ServerSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ServerHostInput = new TextBox();
            AccessTokenInput = new TextBox();
            HostLabel = new Label();
            label1 = new Label();
            SaveButton = new Button();
            SuspendLayout();
            // 
            // ServerHostInput
            // 
            ServerHostInput.BorderStyle = BorderStyle.FixedSingle;
            ServerHostInput.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 134);
            ServerHostInput.Location = new Point(124, 28);
            ServerHostInput.Name = "ServerHostInput";
            ServerHostInput.Size = new Size(374, 28);
            ServerHostInput.TabIndex = 0;
            // 
            // AccessTokenInput
            // 
            AccessTokenInput.BorderStyle = BorderStyle.FixedSingle;
            AccessTokenInput.Font = new Font("Microsoft YaHei UI", 12F);
            AccessTokenInput.Location = new Point(124, 79);
            AccessTokenInput.Name = "AccessTokenInput";
            AccessTokenInput.Size = new Size(263, 28);
            AccessTokenInput.TabIndex = 1;
            // 
            // HostLabel
            // 
            HostLabel.AutoSize = true;
            HostLabel.Location = new Point(35, 34);
            HostLabel.Name = "HostLabel";
            HostLabel.Size = new Size(72, 17);
            HostLabel.TabIndex = 2;
            HostLabel.Text = "Server URL";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(29, 85);
            label1.Name = "label1";
            label1.Size = new Size(87, 17);
            label1.TabIndex = 3;
            label1.Text = "Access Token";
            // 
            // SaveButton
            // 
            SaveButton.Location = new Point(415, 79);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new Size(83, 28);
            SaveButton.TabIndex = 4;
            SaveButton.Text = "保存";
            SaveButton.UseVisualStyleBackColor = true;
            SaveButton.Click += SaveButton_Click;
            // 
            // ServerSetting
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(563, 141);
            Controls.Add(SaveButton);
            Controls.Add(label1);
            Controls.Add(HostLabel);
            Controls.Add(AccessTokenInput);
            Controls.Add(ServerHostInput);
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            Name = "ServerSetting";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "服务端配置";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox ServerHostInput;
        private TextBox AccessTokenInput;
        private Label HostLabel;
        private Label label1;
        private Button SaveButton;
    }
}