﻿namespace Saber_Client.Page
{
    partial class Other
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GithubImg = new PictureBox();
            GitubLink = new LinkLabel();
            CloseButton = new Button();
            OtherLabel = new Label();
            ((System.ComponentModel.ISupportInitialize)GithubImg).BeginInit();
            SuspendLayout();
            // 
            // GithubImg
            // 
            GithubImg.Image = Properties.Resources.github;
            GithubImg.Location = new Point(84, 74);
            GithubImg.Name = "GithubImg";
            GithubImg.Size = new Size(32, 32);
            GithubImg.SizeMode = PictureBoxSizeMode.AutoSize;
            GithubImg.TabIndex = 0;
            GithubImg.TabStop = false;
            // 
            // GitubLink
            // 
            GitubLink.AutoSize = true;
            GitubLink.Location = new Point(122, 82);
            GitubLink.Name = "GitubLink";
            GitubLink.Size = new Size(209, 17);
            GitubLink.TabIndex = 1;
            GitubLink.TabStop = true;
            GitubLink.Text = "https://github.com/xuxianda/saber";
            // 
            // CloseButton
            // 
            CloseButton.Location = new Point(165, 142);
            CloseButton.Name = "CloseButton";
            CloseButton.Size = new Size(75, 23);
            CloseButton.TabIndex = 2;
            CloseButton.Text = "知道了";
            CloseButton.UseVisualStyleBackColor = true;
            CloseButton.Click += CloseButton_Click;
            // 
            // OtherLabel
            // 
            OtherLabel.AutoSize = true;
            OtherLabel.Location = new Point(137, 37);
            OtherLabel.Name = "OtherLabel";
            OtherLabel.Size = new Size(138, 17);
            OtherLabel.TabIndex = 3;
            OtherLabel.Text = "基于Saber开发的客户端";
            // 
            // Other
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            ClientSize = new Size(411, 191);
            ControlBox = false;
            Controls.Add(OtherLabel);
            Controls.Add(CloseButton);
            Controls.Add(GitubLink);
            Controls.Add(GithubImg);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Name = "Other";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "关于";
            ((System.ComponentModel.ISupportInitialize)GithubImg).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private PictureBox GithubImg;
        private LinkLabel GitubLink;
        private Button CloseButton;
        private Label OtherLabel;
    }
}