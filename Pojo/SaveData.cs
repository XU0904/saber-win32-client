﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saber_Client.Pojo
{
    public class SaveData
    {

        public string id { get; set; }

        public string deviceName { get; set; }

        public string deviceType { get; set; }

        public string ipv4 { get; set; }

        public string content { get; set; }

        public DateTime lastTime { get; set; }

    }
}
