﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saber_Client.Pojo
{
    public class HttpResult
    {
        public HttpResult() { }

        public String msg { get; set; }

        public Object data { get; set; }

        public int code { get; set; }
    }
}
