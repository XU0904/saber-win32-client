﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saber_Client.Pojo
{
    public class GlobalData
    {

        public static List<SaveData> SaveDataList;

        public String _serverHost { get; set; }

        public String _accessToken { get; set; }
    }
}
