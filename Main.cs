using Newtonsoft.Json;
using Saber_Client.Page;
using Saber_Client.Pojo;
using System.Data;

namespace Saber_Client
{
    public partial class Main : Form
    {

        public static ServerSetting setting;


        public Main()
        {
            InitializeComponent();
            setting = new ServerSetting();
            globalData = new GlobalData();
            string settingFile = "setting.json";
            if (File.Exists(settingFile))
            {
                string jsonFromFile = File.ReadAllText("setting.json");
                if (!String.IsNullOrEmpty(jsonFromFile))
                {
                    globalData = JsonConvert.DeserializeObject<GlobalData>(jsonFromFile);
                }
            }

        }


        public void SaberHide_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.SaberHide.Visible = false;
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }


        private void 服务端配置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setting = new ServerSetting();
            setting.ShowDialog();
        }

        private void DataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GetAllDeviceInfo_ClickAsync(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(globalData._serverHost))
            {
                MessageBox.Show("Server URL不能为空");
                return;
            }

            if (string.IsNullOrEmpty(globalData._accessToken))
            {
                MessageBox.Show("Access Token不能为空");
                return;
            }

            AsyncTask();
        }


        private async Task AsyncTask()
        {

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("accessToken", globalData._accessToken);
                string requestBody = "{}";
                HttpResponseMessage response = await client.GetAsync(globalData._serverHost);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseBody);
                    HttpResult httpResult = JsonConvert.DeserializeObject<HttpResult>(responseBody);
                    if (httpResult.code == 200)
                    {
                        Console.WriteLine(httpResult.data);
                        List<SaveData> saveDatas = JsonConvert.DeserializeObject<List<SaveData>>(httpResult.data.ToString());
                        Console.WriteLine(saveDatas);



                        //for (int i = 0; i < saveDatas.Count; i++)
                        //{
                        //    SaveData saveData = saveDatas[i];

                        //}


                        DataView.DataSource = saveDatas;
                    }
                }
                else
                {
                    Console.WriteLine("请求失败: " + response.ReasonPhrase);
                }
            }

        }

        Other otherPage;

        private void OtherItem_Click(object sender, EventArgs e)
        {
            otherPage = new Other();
            otherPage.ShowDialog();
        }
    }
}
