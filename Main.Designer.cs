﻿using Saber_Client.Pojo;

namespace Saber_Client
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle14 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle13 = new DataGridViewCellStyle();
            SaberHide = new NotifyIcon(components);
            TopMenu = new MenuStrip();
            StartItem = new ToolStripMenuItem();
            GetAllDeviceInfo = new ToolStripMenuItem();
            Setting = new ToolStripMenuItem();
            服务端配置ToolStripMenuItem = new ToolStripMenuItem();
            DataView = new DataGridView();
            DeviceId = new DataGridViewTextBoxColumn();
            DeviceName = new DataGridViewTextBoxColumn();
            DeviceType = new DataGridViewTextBoxColumn();
            IPSource = new DataGridViewTextBoxColumn();
            Content = new DataGridViewTextBoxColumn();
            LastMsgTime = new DataGridViewTextBoxColumn();
            globalDataBindingSource = new BindingSource(components);
            OtherItem = new ToolStripMenuItem();
            TopMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)DataView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)globalDataBindingSource).BeginInit();
            SuspendLayout();
            // 
            // SaberHide
            // 
            SaberHide.Icon = (Icon)resources.GetObject("SaberHide.Icon");
            SaberHide.Text = "Saber客户端";
            SaberHide.Visible = true;
            SaberHide.MouseDoubleClick += SaberHide_MouseDoubleClick_1;
            // 
            // TopMenu
            // 
            TopMenu.BackColor = SystemColors.Menu;
            TopMenu.Items.AddRange(new ToolStripItem[] { StartItem, Setting, OtherItem });
            TopMenu.Location = new Point(0, 0);
            TopMenu.Name = "TopMenu";
            TopMenu.Size = new Size(1264, 25);
            TopMenu.TabIndex = 0;
            TopMenu.Text = "菜单";
            // 
            // StartItem
            // 
            StartItem.DropDownItems.AddRange(new ToolStripItem[] { GetAllDeviceInfo });
            StartItem.Name = "StartItem";
            StartItem.Size = new Size(44, 21);
            StartItem.Text = "开始";
            // 
            // GetAllDeviceInfo
            // 
            GetAllDeviceInfo.Name = "GetAllDeviceInfo";
            GetAllDeviceInfo.Size = new Size(172, 22);
            GetAllDeviceInfo.Text = "获取所有设备信息";
            GetAllDeviceInfo.Click += GetAllDeviceInfo_ClickAsync;
            // 
            // Setting
            // 
            Setting.BackColor = SystemColors.Control;
            Setting.DropDownItems.AddRange(new ToolStripItem[] { 服务端配置ToolStripMenuItem });
            Setting.Name = "Setting";
            Setting.Size = new Size(44, 21);
            Setting.Text = "设置";
            // 
            // 服务端配置ToolStripMenuItem
            // 
            服务端配置ToolStripMenuItem.Name = "服务端配置ToolStripMenuItem";
            服务端配置ToolStripMenuItem.Size = new Size(180, 22);
            服务端配置ToolStripMenuItem.Text = "服务端配置";
            服务端配置ToolStripMenuItem.Click += 服务端配置ToolStripMenuItem_Click;
            // 
            // DataView
            // 
            DataView.AllowUserToAddRows = false;
            DataView.AllowUserToDeleteRows = false;
            DataView.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            DataView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DataView.BackgroundColor = SystemColors.ControlLightLight;
            DataView.BorderStyle = BorderStyle.None;
            DataView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = SystemColors.Control;
            dataGridViewCellStyle8.Font = new Font("Microsoft YaHei UI", 9F);
            dataGridViewCellStyle8.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = DataGridViewTriState.True;
            DataView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            DataView.ColumnHeadersHeight = 30;
            DataView.Columns.AddRange(new DataGridViewColumn[] { DeviceId, DeviceName, DeviceType, IPSource, Content, LastMsgTime });
            DataView.Cursor = Cursors.No;
            dataGridViewCellStyle14.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = SystemColors.Window;
            dataGridViewCellStyle14.Font = new Font("Microsoft YaHei UI", 9F);
            dataGridViewCellStyle14.ForeColor = SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = DataGridViewTriState.True;
            DataView.DefaultCellStyle = dataGridViewCellStyle14;
            DataView.EditMode = DataGridViewEditMode.EditOnEnter;
            DataView.EnableHeadersVisualStyles = false;
            DataView.GridColor = SystemColors.ActiveBorder;
            DataView.ImeMode = ImeMode.Off;
            DataView.Location = new Point(0, 25);
            DataView.Name = "DataView";
            DataView.ReadOnly = true;
            DataView.Size = new Size(1264, 896);
            DataView.TabIndex = 0;
            DataView.CellContentClick += DataView_CellContentClick;
            // 
            // DeviceId
            // 
            DeviceId.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DeviceId.DataPropertyName = "id";
            dataGridViewCellStyle9.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DeviceId.DefaultCellStyle = dataGridViewCellStyle9;
            DeviceId.FillWeight = 10F;
            DeviceId.HeaderText = "设备ID";
            DeviceId.Name = "DeviceId";
            DeviceId.ReadOnly = true;
            // 
            // DeviceName
            // 
            DeviceName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DeviceName.DataPropertyName = "deviceName";
            dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DeviceName.DefaultCellStyle = dataGridViewCellStyle10;
            DeviceName.FillWeight = 10F;
            DeviceName.HeaderText = "设备名称";
            DeviceName.Name = "DeviceName";
            DeviceName.ReadOnly = true;
            // 
            // DeviceType
            // 
            DeviceType.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DeviceType.DataPropertyName = "deviceType";
            dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DeviceType.DefaultCellStyle = dataGridViewCellStyle11;
            DeviceType.FillWeight = 10F;
            DeviceType.HeaderText = "类型";
            DeviceType.Name = "DeviceType";
            DeviceType.ReadOnly = true;
            // 
            // IPSource
            // 
            IPSource.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            IPSource.DataPropertyName = "ipv4";
            dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleCenter;
            IPSource.DefaultCellStyle = dataGridViewCellStyle12;
            IPSource.FillWeight = 10F;
            IPSource.HeaderText = "IP来源";
            IPSource.Name = "IPSource";
            IPSource.ReadOnly = true;
            // 
            // Content
            // 
            Content.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            Content.DataPropertyName = "content";
            Content.FillWeight = 40F;
            Content.HeaderText = "上报信息";
            Content.MinimumWidth = 150;
            Content.Name = "Content";
            Content.ReadOnly = true;
            Content.Resizable = DataGridViewTriState.True;
            Content.ToolTipText = "点击查看";
            // 
            // LastMsgTime
            // 
            LastMsgTime.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            LastMsgTime.DataPropertyName = "lastTime";
            dataGridViewCellStyle13.Alignment = DataGridViewContentAlignment.MiddleCenter;
            LastMsgTime.DefaultCellStyle = dataGridViewCellStyle13;
            LastMsgTime.FillWeight = 10F;
            LastMsgTime.HeaderText = "最后通讯时间";
            LastMsgTime.Name = "LastMsgTime";
            LastMsgTime.ReadOnly = true;
            // 
            // globalDataBindingSource
            // 
            globalDataBindingSource.DataSource = typeof(GlobalData);
            // 
            // OtherItem
            // 
            OtherItem.Name = "OtherItem";
            OtherItem.Size = new Size(44, 21);
            OtherItem.Text = "关于";
            OtherItem.Click += OtherItem_Click;
            // 
            // Main
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveBorder;
            ClientSize = new Size(1264, 921);
            Controls.Add(DataView);
            Controls.Add(TopMenu);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MainMenuStrip = TopMenu;
            Name = "Main";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Saber V1.0.0";
            Load += Main_Load;
            TopMenu.ResumeLayout(false);
            TopMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)DataView).EndInit();
            ((System.ComponentModel.ISupportInitialize)globalDataBindingSource).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
                SaberHide.Visible = true;
            }
        }



        #endregion

        private NotifyIcon SaberHide;
        private MenuStrip TopMenu;
        private ToolStripMenuItem Setting;
        private ToolStripMenuItem 服务端配置ToolStripMenuItem;
        public static GlobalData globalData = new GlobalData();
        private DataGridView DataView;
        private BindingSource globalDataBindingSource;
        private ToolStripMenuItem StartItem;
        private ToolStripMenuItem GetAllDeviceInfo;
        private DataGridViewTextBoxColumn DeviceId;
        private DataGridViewTextBoxColumn DeviceName;
        private DataGridViewTextBoxColumn DeviceType;
        private DataGridViewTextBoxColumn IPSource;
        private DataGridViewTextBoxColumn Content;
        private DataGridViewTextBoxColumn LastMsgTime;
        private ToolStripMenuItem OtherItem;
    }
}
